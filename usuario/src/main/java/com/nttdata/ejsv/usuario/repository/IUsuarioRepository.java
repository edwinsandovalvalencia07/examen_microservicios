package com.nttdata.ejsv.usuario.repository;

import com.nttdata.ejsv.usuario.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IUsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query(value = "select  * from usuario where active = ?1", nativeQuery = true)
    List<Usuario> listUsuarioActive(Boolean active);
}
