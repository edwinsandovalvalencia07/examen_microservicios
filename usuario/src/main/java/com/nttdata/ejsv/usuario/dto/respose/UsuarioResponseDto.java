package com.nttdata.ejsv.usuario.dto.respose;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioResponseDto {
    private Integer id;
    private String usuario;
    private String clave;
    private Boolean active;
    private Integer idCliente;
//    private EmpleadoResponseDto empleado;


}
