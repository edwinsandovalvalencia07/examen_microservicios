package com.nttdata.ejsv.banco.controller;

import com.nttdata.ejsv.banco.dto.request.BancoRequestDto;
import com.nttdata.ejsv.banco.dto.response.BancoResponseDto;
import com.nttdata.ejsv.banco.service.IBancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/banco")
public class BancoController {
    @Autowired
    private IBancoService iBancoService;

    @GetMapping("/listar")
    public ResponseEntity<List<BancoResponseDto>> list(){
        return  ResponseEntity.ok(iBancoService.listAll());
    }

    @PostMapping("/guardar")
    public ResponseEntity<?> save(@RequestBody BancoRequestDto request){
        return new ResponseEntity<>(this.iBancoService.save(request), HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar/{id}")
    public String delete(@PathVariable("id") Integer id){
        return iBancoService.eliminar(id);
    }

    @PutMapping("/actualizar/{id}")
    public BancoResponseDto actualizar(@RequestBody BancoRequestDto body, @PathVariable("id") Integer id) {
        return iBancoService.update(body, id);
    }
}
