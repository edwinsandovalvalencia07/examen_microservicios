package com.nttdata.ejsv.banco.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BancoRequestDto {
    private Integer id;
    private String nombre;
    private String direccion;
    private Boolean active;
}
