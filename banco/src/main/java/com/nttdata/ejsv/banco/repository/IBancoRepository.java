package com.nttdata.ejsv.banco.repository;


import com.nttdata.ejsv.banco.entity.Banco;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IBancoRepository extends JpaRepository<Banco, Integer> {

}
