package com.nttdata.ejsv.cliente.repository;

import com.nttdata.ejsv.cliente.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IClienteRepository extends JpaRepository<Cliente, Integer> {


}
