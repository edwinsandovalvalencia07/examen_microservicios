package com.nttdata.ejsv.cliente.service;


import com.nttdata.ejsv.cliente.dto.request.ClienteRequestDto;
import com.nttdata.ejsv.cliente.dto.response.ClienteResponseDto;
import com.nttdata.ejsv.cliente.entity.Cliente;
import com.nttdata.ejsv.cliente.repository.IClienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClienteServiceImpl implements IClienteService {
//    private final IEmpresaFeing iEmpresaFeing;

    @Autowired()
    private IClienteRepository iClienteRepository;
    @Override
    public List<ClienteResponseDto> listAll() {
        return this.iClienteRepository.findAll().stream()
                .map(p -> {
                    ClienteResponseDto clienteResponseDto = new ClienteResponseDto();
                    clienteResponseDto.setId(p.getId());
                    clienteResponseDto.setNombres(p.getNombres());
                    clienteResponseDto.setApellidos(p.getApellidos());
                    clienteResponseDto.setSexo(p.getSexo());
                    clienteResponseDto.setActive(p.getActive());

                    return clienteResponseDto;
                }).collect(Collectors.toList());
    }

    @Override
    public ClienteResponseDto save(ClienteRequestDto request) {
//        EmpresaDto empresaDto;
//        empresaDto=iEmpresaFeing.buscar(request.getEmpresaId());
//        if (Objects.nonNull(empresaDto.getMensaje())){
//            throw new RuntimeException("No existe la empresa");
//        }else{
            Cliente cliente = new Cliente();
            cliente.setNombres(request.getNombres());
            cliente.setApellidos(request.getApellidos());
            cliente.setSexo(request.getSexo());
            cliente.setActive(request.getActive());

            this.iClienteRepository.save(cliente);

            ClienteResponseDto response = new ClienteResponseDto();
            response.setNombres(request.getNombres());
            response.setApellidos(request.getApellidos());
            response.setSexo(request.getSexo());
            response.setActive(request.getActive());
            return response;
//        }


    }

    @Override
    public ClienteResponseDto update(ClienteRequestDto request, Integer id) {
        Optional<Cliente> encontrado = iClienteRepository.findById(id);
        Cliente cliente = new Cliente();
        cliente.setNombres(request.getNombres());
        cliente.setApellidos(request.getApellidos());
        cliente.setSexo(request.getSexo());
        cliente.setActive(request.getActive());
        this.iClienteRepository.save(cliente);

        ClienteResponseDto response = new ClienteResponseDto();
        response.setNombres(request.getNombres());
        response.setApellidos(request.getApellidos());
        response.setSexo(request.getSexo());
        response.setActive(request.getActive());
        return response;

    }

    @Override
    public String eliminar(Integer id) {
        Optional<Cliente> encontrado = iClienteRepository.findById(id);
        if (encontrado.isPresent()) {
            iClienteRepository.deleteById(id);
            return "Usuario eliminado correctamente";
        }
        return "Usuario no se encuentra registrado";
    }

    @Override
    public ClienteResponseDto getClienteById(Integer id) {
        ClienteResponseDto responseDto=new ClienteResponseDto();
        Optional<Cliente> cliente=iClienteRepository.findById(id);
        if (cliente.isPresent()) {
            responseDto.setId(cliente.get().getId());
            responseDto.setNombres(cliente.get().getNombres());
            responseDto.setApellidos(cliente.get().getApellidos());
            responseDto.setSexo(cliente.get().getSexo());
            responseDto.setActive(cliente.get().getActive());
        }
        return responseDto;

    }

}
