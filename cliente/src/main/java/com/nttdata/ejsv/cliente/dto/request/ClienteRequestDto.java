package com.nttdata.ejsv.cliente.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteRequestDto {
//    private Integer id;
    private String nombres;
    private String apellidos;
    private String sexo;
    private Boolean active;

}
