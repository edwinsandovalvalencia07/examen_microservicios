package com.nttdata.ejsv.cuentaBancaria.service;


import com.nttdata.ejsv.cuentaBancaria.dto.request.CuentaBancariaRequestDto;
import com.nttdata.ejsv.cuentaBancaria.dto.response.CuentaBancariaResponseDto;

import java.util.List;

public interface ICuentaBancariaService {
    List<CuentaBancariaResponseDto> listAll();

    CuentaBancariaResponseDto save(CuentaBancariaRequestDto request);

    CuentaBancariaResponseDto update(CuentaBancariaRequestDto request,Integer id);

    String eliminar(Integer id);

    CuentaBancariaResponseDto buscarPorNumeroCuenta (String nroCuenta);
}
