package com.nttdata.ejsv.cuentaBancaria.dto.request;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CuentaBancariaRequestDto {
//    private Integer id;
    private String numeroCuenta;
    private String tipoCuenta;
    private Integer idBanco;
    private Integer idCliente;
    private Boolean active;
}
