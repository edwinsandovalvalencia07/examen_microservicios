package com.nttdata.ejsv.cuentaBancaria.controller;

import com.nttdata.ejsv.cuentaBancaria.dto.request.CuentaBancariaRequestDto;
import com.nttdata.ejsv.cuentaBancaria.dto.response.CuentaBancariaResponseDto;
import com.nttdata.ejsv.cuentaBancaria.repository.ICuentaBancariaRepository;
import com.nttdata.ejsv.cuentaBancaria.service.ICuentaBancariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cuenta")
public class CuentaBancariaController {
    @Autowired
    private ICuentaBancariaService iCuentaBancariaService;

    @GetMapping("/listar")
    public ResponseEntity<List<CuentaBancariaResponseDto>> list(){
        return  ResponseEntity.ok(iCuentaBancariaService.listAll());
    }

    @PostMapping("/guardar")
    public ResponseEntity<?> save(@RequestBody CuentaBancariaRequestDto request){
        return new ResponseEntity<>(this.iCuentaBancariaService.save(request), HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar/{id}")
    public String delete(@PathVariable("id") Integer id){
        return iCuentaBancariaService.eliminar(id);
    }

    @PutMapping("/actualizar/{id}")
    public CuentaBancariaResponseDto actualizar(@RequestBody CuentaBancariaRequestDto body, @PathVariable("id") Integer idUsuario) {
        return iCuentaBancariaService.update(body, idUsuario);
    }
}
