package com.nttdata.ejsv.cuentaBancaria.service;


import com.nttdata.ejsv.cuentaBancaria.dto.request.CuentaBancariaRequestDto;
import com.nttdata.ejsv.cuentaBancaria.dto.response.CuentaBancariaResponseDto;
import com.nttdata.ejsv.cuentaBancaria.entity.CuentaBancaria;
import com.nttdata.ejsv.cuentaBancaria.repository.ICuentaBancariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CuentaBancariaServiceImpl implements ICuentaBancariaService {
    @Autowired()
    private ICuentaBancariaRepository iCuentaBancariaRepository;
    @Override
    public List<CuentaBancariaResponseDto> listAll() {
        return this.iCuentaBancariaRepository.findAll().stream()
                .map(p -> {
                    CuentaBancariaResponseDto responseDto = new CuentaBancariaResponseDto();
                    responseDto.setNumeroCuenta(p.getNumeroCuenta());
                    responseDto.setTipoCuenta(p.getTipoCuenta());
                    responseDto.setIdBanco(p.getIdBanco());
                    responseDto.setIdCliente(p.getIdCliente());
                    responseDto.setActive(p.getActive());

                    return responseDto;
                }).collect(Collectors.toList());
    }

    @Override
    public CuentaBancariaResponseDto save(CuentaBancariaRequestDto request) {
        CuentaBancaria cuentaBancaria=new CuentaBancaria();
        cuentaBancaria.setNumeroCuenta(request.getNumeroCuenta());
        cuentaBancaria.setTipoCuenta(request.getTipoCuenta());
        cuentaBancaria.setIdBanco(request.getIdBanco());
        cuentaBancaria.setIdCliente(request.getIdCliente());
        cuentaBancaria.setActive(request.getActive());
        this.iCuentaBancariaRepository.save(cuentaBancaria);

        CuentaBancariaResponseDto responseDto=new CuentaBancariaResponseDto();
        responseDto.setNumeroCuenta(cuentaBancaria.getNumeroCuenta());
        responseDto.setTipoCuenta(cuentaBancaria.getTipoCuenta());
        responseDto.setIdBanco(cuentaBancaria.getIdBanco());
        responseDto.setIdCliente(cuentaBancaria.getIdCliente());
        responseDto.setActive(cuentaBancaria.getActive());
        return responseDto;
    }

    @Override
    public CuentaBancariaResponseDto update(CuentaBancariaRequestDto request, Integer id) {
        Optional<CuentaBancaria> encontrado = iCuentaBancariaRepository.findById(id);

        CuentaBancaria cuentaBancaria=new CuentaBancaria();
        cuentaBancaria.setNumeroCuenta(request.getNumeroCuenta());
        cuentaBancaria.setTipoCuenta(request.getTipoCuenta());
        cuentaBancaria.setIdBanco(request.getIdBanco());
        cuentaBancaria.setIdCliente(request.getIdCliente());
        cuentaBancaria.setActive(request.getActive());
        this.iCuentaBancariaRepository.save(cuentaBancaria);

        CuentaBancariaResponseDto responseDto=new CuentaBancariaResponseDto();
        responseDto.setNumeroCuenta(cuentaBancaria.getNumeroCuenta());
        responseDto.setTipoCuenta(cuentaBancaria.getTipoCuenta());
        responseDto.setIdBanco(cuentaBancaria.getIdBanco());
        responseDto.setIdCliente(cuentaBancaria.getIdCliente());
        responseDto.setActive(cuentaBancaria.getActive());
        return responseDto;
    }

    @Override
    public String eliminar(Integer id) {
        Optional<CuentaBancaria> encontrado = iCuentaBancariaRepository.findById(id);
        if (encontrado.isPresent()) {
            iCuentaBancariaRepository.deleteById(id);
            return "CuentaBancaria eliminado correctamente";
        }
        return "CuentaBancaria no se encuentra registrado";
    }

    @Override
    public CuentaBancariaResponseDto buscarPorNumeroCuenta(String nroCuenta) {

//        CuentaBancariaResponseDto cuentaBancariaResponseDto = new CuentaBancariaResponseDto();
//        CuentaBancaria cuenta=iCuentaBancariaRepository.buscarPorNumeroCuenta(nroCuenta);
//
//            cuentaBancariaResponseDto.setId(cuenta.getId());
//            cuentaBancariaResponseDto.setNumeroCuenta(cuenta.getNumeroCuenta());
//            cuentaBancariaResponseDto.setIdBanco(cuenta.getIdBanco().getIdBanco());
//
//        return cuentaBancariaResponseDto;
        return null;

    }
}
