package com.nttdata.ejsv.cuentaBancaria.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "cuenta_bancaria")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaBancaria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nro_cuenta")
    private String numeroCuenta;
    @Column(name = "tipo_cuenta")
    private String tipoCuenta;
    @Column(name = "id_banco")
    private Integer idBanco;
    @Column(name = "id_cliente")
    private Integer idCliente;
    private Boolean active;


}
